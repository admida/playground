/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { AmdClickcounterComponent } from './amd-clickcounter.component';

describe('AmdClickcounterComponent', () => {
  let component: AmdClickcounterComponent;
  let fixture: ComponentFixture<AmdClickcounterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmdClickcounterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmdClickcounterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

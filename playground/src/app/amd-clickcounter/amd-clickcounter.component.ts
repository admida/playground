import { Component, OnInit } from "@angular/core";

@Component({
    selector: "ns-amd-clickcounter",
    templateUrl: "./amd-clickcounter.component.html",
    styleUrls: ["./amd-clickcounter.component.css"]
})
export class AmdClickcounterComponent implements OnInit {
    clickcounter = 0;

    constructor() {
        // this.clickcounter = 1;
    }

    ngOnInit() {}

    onCount() {
        this.clickcounter++;
    }
}

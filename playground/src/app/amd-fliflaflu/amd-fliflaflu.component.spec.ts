import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmdFliflafluComponent } from './amd-fliflaflu.component';

describe('AmdFliflafluComponent', () => {
  let component: AmdFliflafluComponent;
  let fixture: ComponentFixture<AmdFliflafluComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmdFliflafluComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmdFliflafluComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

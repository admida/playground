import { Component, OnInit } from "@angular/core";

@Component({
    selector: "ns-amd-fliflaflu",
    templateUrl: "./amd-fliflaflu.component.html",
    styleUrls: ["./amd-fliflaflu.component.css"],
})
export class AmdFliflafluComponent {
    player: 0 | 1 | 2;
    computer: number;
    computerP: string;

    min: number;
    max: number;
    result: String = null;

    onPlaying(choice: 0 | 1 | 2) {
        this.computer = this.getRandomIntInclusive(0, 2);
        this.player = choice;

        this.computer === this.player
            ? (this.result = "Unentschieden")
            : this.onCheck();
    }

    onCheck() {
        setTimeout(() => {
            console.log(this.computer);
        }, 500);

        if (this.computer === 0) {
            this.computerP = "Schere";
        } else if (this.computer === 1) {
            this.computerP = "Stein";
        } else {
            this.computerP = "Papier";
        }

        setTimeout(() => {
            if (this.player === 0 && this.computer === 2) {
                this.result = "Gewonnen";
            } else if (this.player === 1 && this.computer === 0) {
                this.result = "Gewonnen";
            } else if (this.player === 2 && this.computer === 1) {
                this.result = "Gewonnen";
            } else {
                this.result = "Verloren";
            }
        }, 1000);

        setTimeout(() => {
            this.result = null;
            this.computerP = null;
        }, 3000);
    }

    getRandomIntInclusive(min: number, max: number) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}

/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { AmdTiktaktoeComponent } from './amd-tiktaktoe.component';

describe('AmdTiktaktoeComponent', () => {
  let component: AmdTiktaktoeComponent;
  let fixture: ComponentFixture<AmdTiktaktoeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmdTiktaktoeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmdTiktaktoeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

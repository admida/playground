import { Component, OnInit } from "@angular/core";

@Component({
    selector: "ns-amd-tiktaktoe",
    templateUrl: "./amd-tiktaktoe.component.html",
    styleUrls: ["./amd-tiktaktoe.component.css"],
})
export class AmdTiktaktoeComponent implements OnInit {
    fieldArray: string[] = [
        "blue",
        "blue",
        "blue",
        "blue",
        "blue",
        "blue",
        "blue",
        "blue",
        "blue",
    ];
    id: string;
    player = "White" || "Black";
    squares: string[];
    winner = "";
    playerC = 1;

    constructor() {}

    ngOnInit() {}

    onClicked(id: string) {
        this.id = id;

        if (this.player === "White") {
            this.player = "Black";
        } else {
            this.player = "White";
        }
        this.fieldArray[this.id] = this.player;
        this.onCheckWin();
        this.playerCount();
    }

    playerCount() {
        this.playerC === 1 ? (this.playerC = 2) : (this.playerC = 1);
    }

    onNewGame() {
        this.fieldArray = Array(9).fill("blue");
        this.winner = "";
    }

    onCheckWin() {
        const lines = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6],
        ];
        for (let i = 0; i < lines.length; i++) {
            const [a, b, c] = lines[i];
            if (
                this.fieldArray[a] === this.fieldArray[b] &&
                this.fieldArray[a] === this.fieldArray[c] &&
                this.fieldArray[a] != "blue"
            ) {
                this.winner = this.fieldArray[a];
                this.winner === "Black"
                    ? (this.winner = "1")
                    : (this.winner = "2");
                // this.winner = this.fieldArray[a];
            }
        }
        return null;
    }
}

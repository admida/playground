import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
    selector: "ns-amd-timer",
    templateUrl: "./amd-timer.component.html",
    styleUrls: ["./amd-timer.component.css"],
})
export class AmdTimerComponent {
    inputValue: number;
    minutes: number;
    seconds: number;
    interval: any;

    onInputChange(time: number) {
        this.inputValue = time;
    }

    onStart() {
        const intervalFunction = () => {
            this.inputValue = this.inputValue - 1;

            let minutes = 0;
            let seconds = 0;

            minutes = this.inputValue / 60;
            minutes = Math.floor(minutes);

            seconds = this.inputValue % 60;

            // minutes = Math.floor(this.inputValue / 60);
            // seconds = this.inputValue % 60;

            this.minutes = minutes;
            this.seconds = seconds;

            if (this.inputValue === 0) {
                clearInterval(this.interval);
            }
            if (this.inputValue < -1) {
                clearInterval(this.interval);
            }
        };
        if (this.interval) {
            clearInterval(this.interval);
        }

        intervalFunction();
        this.interval = setInterval(intervalFunction, 1000);
    }

    onStop() {
        this.inputValue = -2;
    }
}

import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";

import { HomeComponent } from "./home/home.component";
import { AmdClickcounterComponent } from "./amd-clickcounter/amd-clickcounter.component";
import { AmdTimerComponent } from "./amd-timer/amd-timer.component";
import { AmdTiktaktoeComponent } from "./amd-tiktaktoe/amd-tiktaktoe.component";
import { AmdFliflafluComponent } from "./amd-fliflaflu/amd-fliflaflu.component";

const routes: Routes = [
    { path: "", component: HomeComponent },
    { path: "clickcounter", component: AmdClickcounterComponent },
    { path: "timer", component: AmdTimerComponent },
    { path: "tiktaktoe", component: AmdTiktaktoeComponent },
    { path: "fliflaflu", component: AmdFliflafluComponent },
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule],
})
export class AppRoutingModule {}

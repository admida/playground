import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular/side-drawer-directives";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
// import { NgShadowModule } from "nativescript-ng-shadow";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from "nativescript-angular/forms";

// Uncomment and add to NgModule imports if you need to use the HttpClient wrapper
// import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { HomeComponent } from "./home/home.component";
import { AmdButtonComponent } from "./components/amd-button/amd-button.component";
import { AmdActionBarComponent } from "./shared/ui/amd-action-bar/amd-action-bar.component";
import { AmdClickcounterComponent } from "./amd-clickcounter/amd-clickcounter.component";
import { AmdAbListComponent } from "./shared/ui/amd-ab-list/amd-ab-list.component";
import { AmdTimerComponent } from "./amd-timer/amd-timer.component";
import { AmdTiktaktoeComponent } from "./amd-tiktaktoe/amd-tiktaktoe.component";
import { AmdFliflafluComponent } from "./amd-fliflaflu/amd-fliflaflu.component";
import { ButtonComponent } from "./ise-components/button/button.component";
import { SegmentedControlComponent } from "./ise-components/segmentedControl/segmentedControl.component";
import { FabComponent } from "./ise-components/fab/fab.component";

@NgModule({
    bootstrap: [AppComponent],
    imports: [
        NativeScriptModule,
        NativeScriptFormsModule,
        AppRoutingModule,
        NativeScriptUISideDrawerModule,
        // NgShadowModule,
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        AmdButtonComponent,
        AmdActionBarComponent,
        AmdClickcounterComponent,
        AmdAbListComponent,
        AmdTimerComponent,
        AmdTiktaktoeComponent,
        AmdFliflafluComponent,
        ButtonComponent,
        SegmentedControlComponent,
        FabComponent,
    ],
    providers: [],
    schemas: [NO_ERRORS_SCHEMA],
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class AppModule {}

import {
    Component,
    OnInit,
    Input,
    AfterViewInit,
    ViewChild,
    ElementRef,
} from "@angular/core";

import { Button } from "@nativescript/core/ui/button";

@Component({
    selector: "ns-button",
    templateUrl: "./button.component.html",
    styleUrls: ["./button.component.css"],
})
export class ButtonComponent implements OnInit, AfterViewInit {
    @Input() color: string;
    @Input() styling: string;
    @Input() disabled: boolean;
    @ViewChild("button", { read: null, static: false }) button: ElementRef;

    constructor() {}

    ngOnInit() {}

    ngAfterViewInit() {
        const button = <Button>this.button.nativeElement;
        button.addCss("Button {--ise-primary: var(" + this.color + ")}");
    }
}

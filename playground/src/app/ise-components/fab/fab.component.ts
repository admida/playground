import { Component, OnInit, ViewChild, ElementRef, Input } from "@angular/core";
import { screen } from "tns-core-modules/platform";

import { StackLayout } from "@nativescript/core/ui/layouts/stack-layout";

@Component({
    selector: "ns-fab",
    templateUrl: "./fab.component.html",
    styleUrls: ["./fab.component.css"],
})
export class FabComponent implements OnInit {
    @Input() color: string;

    // hier noch zu FABStyle ändern
    @Input() styling: "standard" | "small" | "extended";
    @ViewChild("fab", { read: null, static: false }) fab: ElementRef;

    x: number;

    constructor() {}

    ngOnInit() {
        console.log("Screen heigth: " + screen.mainScreen.heightPixels);
    }

    ngAfterViewInit() {
        // positiert den Fab anhand der Bildschirmgröße
        // nimmt aber den Bildschirm vom PC und nicht emulator
        // geht das wenn es auf dem gespielt wird?

        /* this.x = screen.mainScreen.widthPixels - 40;
        fab.addCss(".float-btn {top: " + this.x + "}"); */

        const fab = <StackLayout>this.fab.nativeElement;

        fab.addCss(".float-btn {background-color: " + this.color + "}");
    }

    onTap() {
        console.log("tapped");
    }
}

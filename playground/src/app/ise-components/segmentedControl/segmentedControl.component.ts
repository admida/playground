import { Component, OnInit } from "@angular/core";

@Component({
    selector: "ns-segmentedControl",
    templateUrl: "./segmentedControl.component.html",
    styleUrls: [
        "./segmentedControl.component.common.css",
        "./segmentedControl.component.css",
    ],
})
export class SegmentedControlComponent implements OnInit {
    constructor() {}

    ngOnInit() {}
}

/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { AmdAbListComponent } from './amd-ab-list.component';

describe('AmdAbListComponent', () => {
  let component: AmdAbListComponent;
  let fixture: ComponentFixture<AmdAbListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmdAbListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmdAbListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

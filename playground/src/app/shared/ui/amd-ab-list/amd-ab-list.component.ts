import { Component, OnInit } from "@angular/core";
import { UIService } from "../ui.service";

@Component({
    selector: "ns-amd-ab-list",
    templateUrl: "./amd-ab-list.component.html",
    styleUrls: ["./amd-ab-list.component.css"],
})
export class AmdAbListComponent implements OnInit {
    items: any;

    constructor(private uiService: UIService) {
        this.items = [
            {
                title: "Home",
                path: "",
            },
            {
                title: "Clickcounter",
                path: "clickcounter",
            },
            {
                title: "Timer",
                path: "timer",
            },
            {
                title: "Tik-tak-toe",
                path: "tiktaktoe",
            },
            {
                title: "Fli-Fla-Flu",
                path: "fliflaflu",
            },
        ];
    }

    ngOnInit() {}

    onToggleMenu() {
        this.uiService.toggleDrawer();
    }
}

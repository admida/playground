import { Component, OnInit, Input } from "@angular/core";
import { isAndroid } from "../../../../../node_modules/tns-core-modules/platform/platform";
import { UIService } from "../ui.service";
import { Page } from "tns-core-modules/ui/page/page";

declare var android: any;

@Component({
    selector: "ns-amd-action-bar",
    templateUrl: "./amd-action-bar.component.html",
    styleUrls: ["./amd-action-bar.component.css"]
})
export class AmdActionBarComponent implements OnInit {
    @Input() title: string;

    constructor(private page: Page, private uiService: UIService) {}

    ngOnInit() {}

    get android() {
        return isAndroid;
    }

    onToggleMenu() {
        this.uiService.toggleDrawer();
    }
}
